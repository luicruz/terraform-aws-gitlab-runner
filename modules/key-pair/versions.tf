terraform {
  required_version = "= 0.12.29"
  required_providers {
    aws   = "= 2.70"
    local = ">= 1.4"
    tls   = ">= 2"
    null  = ">= 2"
  }
}
